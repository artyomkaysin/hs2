function getXmlHttp()
{
    var xmlhttp;
    try
    {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (e)
    {
        try
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        catch (E)
        {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest!='undefined')
    {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}

function refreshTable()
{
    var req = getXmlHttp()
    req.onreadystatechange = function()
    {
        if (req.readyState == 4)
        {
            if(req.status == 200)
            {
                document.getElementById('Students').innerHTML=req.responseText;

                //Присваиваем шапке фиксированную ширину после загрузки контента
                document.getElementById("TheadNumber1").style.width = document.getElementById("TheadNumber1").offsetWidth+"px";
                document.getElementById("TheadClass1").style.width = document.getElementById("TheadClass1").offsetWidth+"px";
                document.getElementById("TheadName1").style.width = document.getElementById("TheadName1").offsetWidth+"px";
                document.getElementById("TheadDot1").style.width = document.getElementById("TheadDot1").offsetWidth+"px";
                document.getElementById("TheadDateBirth1").style.width = document.getElementById("TheadDateBirth1").offsetWidth+"px";

                //Дублируем ширину фиксированной шапке
                document.getElementById("TheadNumber2").style.width = document.getElementById("TheadNumber1").offsetWidth+"px";
                document.getElementById("TheadClass2").style.width = document.getElementById("TheadClass1").offsetWidth+"px";
                document.getElementById("TheadName2").style.width = document.getElementById("TheadName1").offsetWidth+"px";
                document.getElementById("TheadDot2").style.width = document.getElementById("TheadDot1").offsetWidth+"px";
                document.getElementById("TheadDateBirth2").style.width = document.getElementById("TheadDateBirth1").offsetWidth+"px";
            }
        }
    }
    req.open('POST', '/application/scripts/refresh_table.php', true);
    req.send(null);
}

function changeValue(str)
{
    var req = getXmlHttp()
    req.onreadystatechange = function()
    {
        if (req.readyState == 4)
        {
            if(req.status == 200)
            {
            refreshTable();
            }
        }
    }
    var l = str.indexOf("|");
    var id = str.substring(0, l);
    l++;
    var type = str.substring(l);
    var text = event.target.innerHTML;
    var query='id='+id+'&type='+type+'&text='+text;

    req.open('POST', '/application/scripts/change_value.php', true);
    req.setRequestHeader('Content-type','application/x-www-form-urlencoded');
    req.send(query);
}

function checkValue(str)
{
    var text="";
    if (str=="Number")
    {
        document.getElementById("ErrorNumber").innerHTML = "";
        text = document.getElementById("FormNumber").value;
        var re = /^[0-9]+$/;
        if (!re.test(text))
        {
            document.getElementById("ErrorNumber").innerHTML = "Может принимать только числовые значения.";
            return false;
        }
        else
        {
            return true;
        }
    }
    else if (str=="Class")
    {
        document.getElementById("ErrorClass").innerHTML = "";
        text = document.getElementById("FormClass").value;
        var re = /^[А-Я0-9]+$/;
        if (!re.test(text))
        {
            document.getElementById("ErrorClass").innerHTML = "Может принимать данные в формате номер и буква класса без пробелов.";
            return false;
        }
        else
        {
            return true;
        }
    }
    else if (str=="Name")
    {
        document.getElementById("ErrorName").innerHTML = "";
        text = document.getElementById("FormName").value;
        var re = /^[а-яА-Я ]+$/;
        if (!re.test(text))
        {
            document.getElementById("ErrorName").innerHTML = "Может принимать только кириллические значения с пробелами";
            return false;
        }
        else
        {
            return true;
        }
    }
    else if (str=="Dot")
    {
        document.getElementById("ErrorDot").innerHTML = "";
        text = document.getElementById("FormDot").value;
        if (text=="Да"||text=="Нет")
        {
            return true;
        }
        else
        {
            document.getElementById("ErrorDot").innerHTML = "Может принимать значения только \"Да\" и \"Нет\".";
            return false;
        }
    }
    else if (str=="DateBirth")
    {
        document.getElementById("ErrorDateBirth").innerHTML = "";
        text = document.getElementById("FormDateBirth").value;
        var arrD = text.split(".");
        arrD[1] -= 1;
        var d = new Date(arrD[2], arrD[1], arrD[0]);
        if ((d.getFullYear() == arrD[2]) && (d.getMonth() == arrD[1]) && (d.getDate() == arrD[0]))
        {
            return true;
        }
        else
        {
            document.getElementById("ErrorDateBirth").innerHTML = "Может принимать значения только в формате \"ДД.ММ.ГГГГ\".";
            return false;
        }
    }
    else
    {
        alert("Неизвестный параметр для проверки");
    }
}

function addStudent()
{

    var req = getXmlHttp()
    req.onreadystatechange = function()
    {
        if (req.readyState == 4)
        {
            if(req.status == 200)
            {
                refreshTable();
            }
        }
    }

    var checkNumber = checkValue("Number");
    var checkClass = checkValue("Class");
    var checkName = checkValue("Name");
    var checkDot = checkValue("Dot");
    var checkDateBirth = checkValue("DateBirth");

    if (checkNumber==true&&checkClass==true&&checkName==true&&checkDot==true&&checkDateBirth==true)
    {
        var queryNumber = document.getElementById("FormNumber").value;
        var queryClass = document.getElementById("FormClass").value;
        var queryName = document.getElementById("FormName").value;
        var queryDot = document.getElementById("FormDot").value;
        var queryDateBirth = document.getElementById("FormDateBirth").value;

        var query='Number='+queryNumber+'&Class='+queryClass+'&Name='+queryName+'&Dot='+queryDot+'&DateBirth='+queryDateBirth;

        req.open('POST', '/application/scripts/add_student.php', true);
        req.setRequestHeader('Content-type','application/x-www-form-urlencoded');
        req.send(query);
    }
    else
    {
        return false;
    }
}