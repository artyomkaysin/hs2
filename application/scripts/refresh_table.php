<?php
/**
 * Created by PhpStorm.
 * User: Артём
 * Date: 18.03.2019
 * Time: 19:57
 */

require_once $_SERVER['DOCUMENT_ROOT']."/application/models/model_mysql.php";
$result = Query::getQuery("SELECT * FROM `Students`");

while ($row = mysqli_fetch_assoc($result))
{
    $id = $row['Id'];
    echo "
        <tr>
            <td contenteditable='true' class='lg' onBlur='changeValue(\"$id|Number\")'>".$row['Number']."</td>
            <td contenteditable='true' class='lg' onBlur='changeValue(\"$id|Class\")'>".$row['Class']."</td>
            <td contenteditable='true' class='lg' onBlur='changeValue(\"$id|Name\")'>".$row['Name']."</td>
            <td contenteditable='true' class='lg' onBlur='changeValue(\"$id|Dot\")'>".$row['Dot']."</td>
            <td contenteditable='true' class='lg' onBlur='changeValue(\"$id|DateBirth\")'>".$row['DateBirth']."</td>
        </tr>
        <tr class='sm'>
            <td>
                <div class='sm'>
                    Номер: <span contenteditable='true' onBlur='changeValue(\"$id|Number\")'>".$row['Number']."</span><br>
                    Класс: <span contenteditable='true' onBlur='changeValue(\"$id|Class\")'>".$row['Class']."</span><br>
                    Ученик: <span contenteditable='true' onBlur='changeValue(\"$id|Name\")'>".$row['Name']."</span><br>
                    Дотация: <span contenteditable='true' onBlur='changeValue(\"$id|Dot\")'>".$row['Dot']."</span><br>
                    Дата рождения: <span contenteditable='true' onBlur='changeValue(\"$id|DateBirth\")'>".$row['DateBirth']."</span>
                </div>
            </td>
        </tr>
        ";
}
