<?php
/**
 * Created by PhpStorm.
 * User: Артём
 * Date: 18.03.2019
 * Time: 14:39
 */

require_once 'core/model.php';
require_once 'core/view.php';
require_once 'core/controller.php';
require_once 'core/route.php';
Route::start(); // запускаем маршрутизатор
