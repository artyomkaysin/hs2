<?php
/*
* Mysql database class - only one connection alowed
*/
class DB
{
    private $_connection;
    private static $_instance; //The single instance

    /*
    Get an instance of the Database
    @return Instance
    */
    public static function getInstance() {
        if(!self::$_instance) { // If no instance then make one
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    // Constructor
    private function __construct() {
        include_once $_SERVER['DOCUMENT_ROOT']."/conf/connetion.php";
        $this->_connection = new mysqli($host, $user, $password, $database);

        // Error handling
        if(mysqli_connect_error()) {
            trigger_error("Failed to conencto to MySQL: " . mysql_connect_error(),
                E_USER_ERROR);
        }
    }
    // Magic method clone is empty to prevent duplication of connection
    private function __clone() { }
    // Get mysqli connection
    public function getConnection() {
        return $this->_connection;
    }
}

class Query
{
    public static function getQuery($sql_query)
    {
        $db = DB::getInstance();
        $mysqli = $db->getConnection();

        $sql_result = $mysqli->query($sql_query);
        return $sql_result;
    }
}
