<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>Задание Кайсин А.И.</title>
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <script type="text/javascript" src="/js/main.js"></script>
</head>
<body>
<?php include 'application/views/'.$content_view; ?>
</body>
</html>
