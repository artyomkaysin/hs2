<div class="area">
    <div class="row">
        <div class="table">
            <table>
                <thead>
                    <tr>
                        <td id="TheadNumber1">Номер</td>
                        <td id="TheadClass1">Класс</td>
                        <td id="TheadName1">Ученик</td>
                        <td id="TheadDot1">Дотация</td>
                        <td id="TheadDateBirth1">Дата рождения</td>
                    </tr>
                </thead>
                <tbody id="Students">
                    <script type="text/javascript">
                        refreshTable();
                    </script>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="form">
            <form onSubmit="addStudent(); return false;">
                <br>
                Номер:
                <br>
                <input type="text" id="FormNumber" value="" onBlur="checkValue('Number')">
                <br>
                <span id="ErrorNumber"></span>
                <br>
                Класс:
                <br>
                <input type="text" id="FormClass" value="" onBlur="checkValue('Class')">
                <br>
                <span id="ErrorClass"></span>
                <br>
                Имя:
                <br>
                <input type="text" id="FormName" value="" onBlur="checkValue('Name')">
                <br>
                <span id="ErrorName"></span>
                <br>
                Дотация:
                <br>
                <input type="text" id="FormDot" value="" onBlur="checkValue('Dot')">
                <br>
                <span id="ErrorDot"></span>
                <br>
                Дата рождения:
                <br>
                <input type="text" id="FormDateBirth" value="" onBlur="checkValue('DateBirth')">
                <br>
                <span id="ErrorDateBirth"></span>
                <br>
                <input type="submit" value="Отправить.">
            </form>
        </div>
    </div>
</div>
<div class="table thead-fixed" id="theadFixed">
    <table>
        <thead>
        <tr>
            <td id="TheadNumber2">Номер</td>
            <td id="TheadClass2">Класс</td>
            <td id="TheadName2">Ученик</td>
            <td id="TheadDot2">Дотация</td>
            <td id="TheadDateBirth2">Дата рождения</td>
        </tr>
        </thead>
    </table>
</div>
<script type="text/javascript">

    window.onscroll = function() {
        var scrolled = window.pageYOffset || document.documentElement.scrollTop;
        if (scrolled > 30) {
            document.getElementById('theadFixed').style.display = 'block';
        } else {
            document.getElementById('theadFixed').style.display = 'none';
        }
    }
</script>
